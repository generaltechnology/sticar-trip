<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Carbon\Carbon;
use DB;

class LolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'List LOLA';

        $this->data['lolas'] = (
            DB::table('Lola')
                ->join('LocationArea', 'Lola.areaId', '=', 'LocationArea.areaId')
                ->select([
                    'Lola.lolaId',
                    'Lola.areaId',
                    'LocationArea.areaName'
                ])
                ->groupBy('Lola.lolaId')
                ->orderBy('LocationArea.areaName')
                ->get()
        );

        return view('lola.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'Add LOLA';

        $lolas = (
            DB::table('Lola')
                ->join('LocationArea', 'Lola.areaId', '=', 'LocationArea.areaId')
                ->select([
                    'Lola.lolaId',
                    'Lola.areaId',
                    'LocationArea.areaName'
                ])
                ->groupBy('Lola.lolaId')
                ->pluck('areaId')
        );

        $this->data['locAreas'] = (
            DB::table('LocationArea')
                ->whereNotIn('areaId', $lolas)
                ->orderBy('areaName', 'ASC')
                ->get()
        );

        return view('lola.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('Lola')->insert([
            'areaId'  => $request->areaId,
            'latMin'  => $request->latMin,
            'latMax'  => $request->latMax,
            'longMin' => $request->longMin,
            'longMax' => $request->longMax
        ]);

        return redirect()->route('lola');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['title'] = 'Edit LOLA';

        $this->data['lola'] = (
            DB::table('Lola')
                ->join('LocationArea', 'Lola.areaId', '=', 'LocationArea.areaId')
                ->select([
                    'Lola.lolaId',
                    'Lola.areaId',
                    'LocationArea.areaName',
                    'Lola.latMin',
                    'Lola.longMin',
                    'Lola.latMax',
                    'Lola.longMax'
                ])
                ->where('Lola.lolaId', $id)
                ->first()
        );

        $lolas = (
            DB::table('Lola')
                ->whereNotIn('areaId', [$this->data['lola']->areaId])
                ->pluck('areaId')
        );

        $this->data['locAreas'] = (
            DB::table('LocationArea')
                ->whereNotIn('areaId', $lolas)
                ->orderBy('areaName', 'ASC')
                ->get()
        );

        return view('lola.edit', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('Lola')
            ->where('lolaId', $id)
            ->update([
                'areaId'    => $request->areaId,
                'latMin'    => $request->latMin,
                'latMax'    => $request->latMax,
                'longMin'   => $request->longMin,
                'longMax'   => $request->longMax,
                'updatedAt' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

        return redirect()->route('lola');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
