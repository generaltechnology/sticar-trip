<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->data = [
            'today'      => Carbon::now()->format('Y-m-d'),
            'now'        => Carbon::now()->format('Y-m-d H:i:s'),
            'startToday' => Carbon::now()->format('Y-m-d 00:00:00'),
            'endToday'   => Carbon::now()->format('Y-m-d 23:59:59')
        ];
    }

    public function index()
    {
        $this->data['title'] = 'All Campaign';

        $active = (
            DB::table('Campaign')
                ->select([
                    'Campaign.campaignId',
                    'Campaign.campaignName',
                    'Campaign.startDate',
                    'Campaign.endDate'
                ])
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('Contract')
                        ->whereRaw('Contract.campaignId = Campaign.campaignId')
                        ->where('status', 'approved');
                })
                ->where('Campaign.startDate', '<=', Carbon::now()->addMonth()->format('Y-m-d'))
                ->where('Campaign.endDate', '>', Carbon::now()->format('Y-m-d'))
                ->where('Campaign.status', 'active')
                ->orderBy('Campaign.startDate', 'ASC')
                ->groupBy('Campaign.campaignId')
                ->get()
        );

        $ended = (
            DB::table('Campaign')
                ->select([
                    'Campaign.campaignId',
                    'Campaign.campaignName',
                    'Campaign.startDate',
                    'Campaign.endDate'
                ])
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('Contract')
                        ->whereRaw('Contract.campaignId = Campaign.campaignId')
                        ->where('status', 'approved');
                })
                ->where('Campaign.endDate', '<', Carbon::now()->format('Y-m-d'))
                ->where('Campaign.endDate', '>', Carbon::now()->subMonth()->format('Y-m-d'))
                ->where('Campaign.status', 'active')
                ->orderBy('Campaign.endDate', 'ASC')
                ->groupBy('Campaign.campaignId')
                ->get()
        );

        $campaigns = $active->merge($ended);
        // dd($campaigns->pluck('campaignId'));

        foreach ($campaigns as $key => $campaign) {
            $contractIds = (
                DB::table('Contract')
                    ->where('campaignId', $campaign->campaignId)
                    ->where('status', 'approved')
                    ->groupBy('contractId')
                    ->pluck('contractId')
            );

            $online = (
                DB::table('Trip')
                    ->whereIn('contractId', $contractIds)
                    ->where('latitude', '!=', 0)
                    ->where('longitude', '!=', 0)
                    ->where('status', '!=', 'closed')
                    ->where('createdAt', '>=', $this->data['today'])
                    ->groupBy('contractId')
                    ->pluck('contractId')
            );

            $percentage = round(count($online) / count($contractIds) * 100);

            if ($percentage >= 75 && $percentage <= 100) {
                $status = 'table-success';
            } elseif ($percentage >= 50 && $percentage < 75) {
                $status = 'table-primary';
            } elseif ($percentage >= 25 && $percentage < 50) {
                $status = 'table-warning';
            } else {
                $status = 'table-danger';
            }

            if ($campaign->endDate < Carbon::now()->format('Y-m-d')) {
                $status = 'table-light';
            }

            $this->data['campaigns'][] = [
                'name'       => $campaign->campaignName,
                'id'         => $campaign->campaignId,
                'online'     => count($online),
                'total'      => count($contractIds),
                'percentage' => $percentage,
                'startDate'  => $campaign->startDate,
                'endDate'    => $campaign->endDate,
                'status'     => $status
            ];
        }

        return view('home', $this->data);
    }
}
