<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Sticar | {{ $title }}</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">

        <style type="text/css" media="screen">
            body {
                font-size: 12px;
                font-family: sans-serif;
            }

            button[disabled],
            input[disabled] {
                cursor: not-allowed;
            }
        </style>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#report').DataTable();
            });

            function inject() {
                date = $("input[name='date']").val();

                if (date) {
                    if (confirm('Inject KM on ' + date + '?')) {
                        console.log(date);
                        $('#closeModal').fadeOut();
                        $('#injectTrip i').removeClass('fas fa-play').addClass('fas fa-circle-notch fa-spin');
                        $('#injectTrip').attr('disabled', true);
                        $("input[name='date']").attr('disabled', true);

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            url: '{!! route('report-inject', ['id' => $campaignId]) !!}',
                            data: {
                                date: date
                            },
                            success: function(res) {
                                if (res.status === true) {
                                    location.reload();
                                } else {
                                    alert(res.message);
                                    $('#injectTrip i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-play');
                                    $('#injectTrip').attr('disabled', false);
                                }
                            }
                        });
                    }
                }
            }
        </script>
    </head>
    <body>
        <div class="jumbotron my-0 py-3">
            <h1 class="display-4 m-0 text-right"><strong>{{ $title }}</strong></h1>
            <h3 class="display-5 m-0 text-right">
                <div class="float-left">
                    Report from (<strong>{{ date('d M Y', strtotime($startDate)) }}</strong> -  <strong>{{ date('d M Y', strtotime($endDate)) }}</strong>)
                </div>
                <a href="{{ url()->full() }}" class="btn btn-sm btn-info" role="button" aria-pressed="true"><i class="fas fa-retweet"></i> Refresh</a>
                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#injectModal"><i class="fas fa-play"></i> Inject KM</button>
                <a href="{{ route('campaign', ['id' => $campaignId]) }}" class="btn btn-sm btn-danger" role="button" aria-pressed="true"><i class="fas fa-backward"></i> Back</a>
                <a href="{{ route('home') }}" class="btn btn-sm btn-primary" role="button" aria-pressed="true"><i class="fas fa-home"></i> Home</a>
            </h3>
        </div>
        <div class="container-fluid py-4">
            <div class="row">
                <table id="report" class="table table-bordered table-hover" style="width:100%">
                    <thead class="thead-light">
                        <tr>
                            <th width="25">No</th>
                            <th>Date</th>
                            <th>Total KM</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reports as $key => $report)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td data-order="{{ $report->date }}">{{ date('d M Y', strtotime($report->date)) }}</td>
                                <td>{{ round($report->totalDistance / 1000) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="injectModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title m-0"><strong><u><i>Inject KM</i></u></strong></h3>
                    </div>
                    <div class="modal-body">
                        <label><h5 class="m-0">Date:</h5></label>
                        <input type="date" class="form-control" name="date" min="{{ $startDate }}" max="{{ $maxDate }}">
                    </div>
                    <div class="modal-footer">
                        <button id="closeModal" type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                        <button id="injectTrip" type="button" class="btn btn-sm btn-success" onclick="inject()"><i class="fas fa-play"></i> Inject Trip</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
