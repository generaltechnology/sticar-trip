<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Sticar | {{ $title }}</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">

        <style type="text/css" media="screen">
            body {
                font-size: 12px;
                font-family: sans-serif;
            }
            tr.strikeout td {
                text-decoration: line-through;
            }
        </style>

        <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#campaign').DataTable({
                    // 'order': [[3, 'desc']]
                });
            });
        </script>
    </head>
    <body>
        <div class="jumbotron my-0 py-3">
            <h1 class="display-4 m-0 text-right"><strong>{{ $title }}</strong></h1>
            <h3 class="display-5 m-0 text-right">

                <a href="{{ route('lola-create') }}" class="btn btn-sm btn-success" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Add LOLA</a>
                <a href="{{ route('home') }}" class="btn btn-sm btn-primary" role="button" aria-pressed="true"><i class="fas fa-home"></i> Home</a>
            </h3>
        </div>
        <div class="container-fluid py-4">
            <div class="row">
                <table id="campaign" class="table table-bordered table-hover" style="width:100%">
                    <thead class="thead-light">
                        <tr>
                            <th width="25">No</th>
                            <th>Area Name</th>
                            <th class="text-center" width="50">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lolas as $key => $lola)
                            <tr>
                                <td class="align-middle">{{ ++$key }}</td>
                                <td class="align-middle">{{ $lola->areaName }}</td>
                                <td class="align-middle text-center">
                                    <a class="btn btn-sm btn-primary" href="{{ route('lola-show', ['id' => $lola->lolaId] ) }}" role="button"><i class="fas fa-external-link-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
