<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Sticar | {{ $title }}</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">

        <style type="text/css" media="screen">
            body {
                font-size: 12px;
                font-family: sans-serif;
            }
            tr.strikeout td {
                text-decoration: line-through;
            }
        </style>

        <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    </head>
    <body>
        <div class="jumbotron my-0 py-3">
            <h1 class="display-4 m-0 text-right"><strong>{{ $title }}</strong></h1>
            <h3 class="display-5 m-0 text-right">

                <a href="{{ route('lola') }}" class="btn btn-sm btn-danger" role="button" aria-pressed="true"><i class="fas fa-backward"></i> Back</a>
            </h3>
        </div>
        <div class="container py-4">
            <form id="form" method="POST" action="{{ route('lola-store') }}" enctype="multipart/form-data">

                {{ method_field('POST') }}
                {{ csrf_field() }}

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" style="font-size: 16px;">Area Name</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="areaId" required>
                            <option></option>
                            @foreach ($locAreas as $key => $locArea)
                                <option value="{{ $locArea->areaId }}">{{ $locArea->areaName }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Lat Min</label>
                            <input type="text" class="form-control" name="latMin" required>
                        </div>
                    </div>
                    <div class="col-2" style="padding: 0;">
                        <div class="divider" style="position: absolute; bottom: 1rem; text-align: center; font-size: 24px; width: 100%;">
                            ,
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Long Min</label>
                            <input type="text" class="form-control" name="longMin" required>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Lat Max</label>
                            <input type="text" class="form-control" name="latMax" required>
                        </div>
                    </div>
                    <div class="col-2" style="padding: 0;">
                        <div class="divider" style="position: absolute; bottom: 1rem; text-align: center; font-size: 24px; width: 100%;">
                            ,
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Long Max</label>
                            <input type="text" class="form-control" name="longMax" required>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><i class="fas fa-plus"></i> Create</button>
                </div>
            </form>
        </div>
    </body>
</html>
