<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="{{ secure_asset('favicon.ico') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Sticar | {{ $title }}</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">

        <style type="text/css" media="screen">
            body {
                font-size: 12px;
                font-family: sans-serif;
            }
            tr.strikeout td {
                text-decoration: line-through;
            }
        </style>

        <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#campaign').DataTable({
                    // 'order': [[3, 'desc']]
                });
            });
        </script>
    </head>
    <body>
        <div class="jumbotron my-0 py-3">
            <h1 class="display-4 m-0 text-right"><strong>{{ $title }}</strong></h1>
            <h3 class="display-5 m-0 text-right">
                <a href="{{ route('lola') }}" class="btn btn-sm btn-primary" role="button" aria-pressed="true"><i class="fas fa-map-marker"></i> LOLA</a>
                <a href="{{ route('home') }}" class="btn btn-sm btn-success" role="button" aria-pressed="true"><i class="fas fa-sync-alt"></i> Refresh</a>
            </h3>
        </div>
        <div class="container-fluid py-4">
            <div class="row">
                <table id="campaign" class="table table-bordered table-hover" style="width:100%">
                    <thead class="thead-light">
                        <tr>
                            <th width="25">No</th>
                            <th>Name</th>
                            <th class="text-center" width="125">Start Date</th>
                            <th class="text-center" width="125">End Date</th>
                            <th class="text-center" width="75">On Trip</th>
                            <th class="text-center" width="75">Total</th>
                            <th class="text-center" width="100">Percentage</th>
                            <th class="text-center" width="50">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($campaigns as $key => $campaign)
                            <tr class="{{ $campaign['status'] . ($campaign['status'] === 'table-light' ? ' strikeout' : '') }}">
                                <td class="align-middle">{{ ++$key }}</td>
                                <td class="align-middle">{{ $campaign['name'] }}</td>
                                <td class="align-middle text-center" data-order="{{ $campaign['startDate'] }}">{{ date('d M Y', strtotime($campaign['startDate'])) }}</td>
                                <td class="align-middle text-center" data-order="{{ $campaign['endDate'] }}">{{ date('d M Y', strtotime($campaign['endDate'])) }}</td>
                                <td class="align-middle text-center">{{ $campaign['online'] }}</td>
                                <td class="align-middle text-center">{{ $campaign['total'] }}</td>
                                <td class="align-middle text-center">{{ $campaign['percentage'] }}%</td>
                                <td class="align-middle text-center">
                                    <a class="btn btn-sm btn-primary" href="{{ url('campaign/' . $campaign['id'] ) }}" role="button"><i class="fas fa-external-link-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
