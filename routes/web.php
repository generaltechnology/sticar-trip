<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/campaign/{id}', 'CampaignController@index')->name('campaign');
Route::post('/campaign/{id}/start', 'CampaignController@start')->name('campaign-start');
Route::get('/campaign/{id}/report', 'CampaignController@report')->name('report');
Route::post('/campaign/{id}/inject', 'CampaignController@inject')->name('report-inject');

Route::get('/lola', 'LolaController@index')->name('lola');
Route::get('/lola/create', 'LolaController@create')->name('lola-create');
Route::post('/lola/store', 'LolaController@store')->name('lola-store');
Route::get('/lola/{id}', 'LolaController@show')->name('lola-show');
Route::post('/lola/{id}/update', 'LolaController@update')->name('lola-update');
